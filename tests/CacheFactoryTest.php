<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Test;

use Blazon\PSR11SymfonyCache\Adapter\AdapterMapper;
use Blazon\PSR11SymfonyCache\CacheFactory;
use Blazon\PSR11SymfonyCache\Config\Config;
use Blazon\PSR11SymfonyCache\Exception\InvalidConfigException;
use Blazon\PSR11SymfonyCache\Exception\InvalidContainerException;
use Blazon\PSR11SymfonyCache\Exception\MissingConfigException;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\DependencyInjection\Container as SymfonyContainer;

/**
 * @covers \Blazon\PSR11SymfonyCache\CacheFactory
 */
class CacheFactoryTest extends TestCase
{
    public function testGetConfigArraySymfony()
    {
        $expected = [
            'cache' => [
                'some-key' => 'some-value',
                'some-other-key' => 'some-other-value',
            ]
        ];

        $mockContainer = $this->getMockBuilder(SymfonyContainer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockContainer->expects($this->once())
            ->method('hasParameter')
            ->with($this->equalTo('cache'))
            ->willReturn(true);

        $mockContainer->expects($this->once())
            ->method('getParameter')
            ->with($this->equalTo('cache'))
            ->willReturn($expected['cache']);

        $factory = new CacheFactory();
        $result = $factory->getConfigArray($mockContainer);

        $this->assertEquals($expected, $result);
    }

    public function testGetConfigArrayZend()
    {
        $expected = [
            'cache' => [
                'some-key' => 'some-value',
                'some-other-key' => 'some-other-value',
            ]
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);

        $hasMap = [
            ['config', true],
            ['settings', false],
        ];

        $mockContainer->expects($this->atLeastOnce())
            ->method('has')
            ->willReturnMap($hasMap);

        $mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('config'))
            ->willReturn($expected);

        $factory = new CacheFactory();
        $result = $factory->getConfigArray($mockContainer);

        $this->assertEquals($expected, $result);
    }

    public function testGetConfigArraySlim()
    {
        $expected = [
            'cache' => [
                'some-key' => 'some-value',
                'some-other-key' => 'some-other-value',
            ]
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);

        $hasMap = [
            ['config', false],
            ['settings', true],
        ];

        $mockContainer->expects($this->atLeastOnce())
            ->method('has')
            ->willReturnMap($hasMap);

        $mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('settings'))
            ->willReturn($expected);

        $factory = new CacheFactory();
        $result = $factory->getConfigArray($mockContainer);

        $this->assertEquals($expected, $result);
    }

    public function testGetConfigArrayMissing()
    {
        $this->expectException(MissingConfigException::class);
        $mockContainer = $this->createMock(ContainerInterface::class);

        $hasMap = [
            ['config', false],
            ['settings', false],
        ];

        $mockContainer->expects($this->atLeastOnce())
            ->method('has')
            ->willReturnMap($hasMap);

        $mockContainer->expects($this->never())
            ->method('get');

        $factory = new CacheFactory();
        $factory->getConfigArray($mockContainer);
    }

    public function testGetConfig()
    {
        $expected = [
            'cache' => [
                'default' => [
                    'type' => 'array',
                    'options' => []
                ],
            ],
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);

        $hasMap = [
            ['config', true],
            ['settings', false],
        ];

        $mockContainer->expects($this->atLeastOnce())
            ->method('has')
            ->willReturnMap($hasMap);

        $mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('config'))
            ->willReturn($expected);

        $factory = new CacheFactory();
        $result = $factory->getConfig($mockContainer);

        $this->assertInstanceOf(Config::class, $result);
    }

    public function testGetConfigWithServiceName()
    {
        $serviceKey = 'my-service-name';
        $expected = [
            'cache' => [
                $serviceKey => [
                    'type' => 'memory',
                    'options' => []
                ],
            ],
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);

        $hasMap = [
            ['config', true],
            ['settings', false],
        ];

        $mockContainer->expects($this->atLeastOnce())
            ->method('has')
            ->willReturnMap($hasMap);

        $mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('config'))
            ->willReturn($expected);

        $factory = new CacheFactory($serviceKey);
        $result = $factory->getConfig($mockContainer);

        $this->assertInstanceOf(Config::class, $result);
    }

    public function testGetConfigWithServiceNameMissingConfig()
    {
        $this->expectException(InvalidConfigException::class);
        $serviceKey = 'my-service-name';
        $expected = [
            'cache' => [
                'default' => [
                    'type' => 'memory',
                    'options' => []
                ],
            ],
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);

        $hasMap = [
            ['config', true],
            ['settings', false],
        ];

        $mockContainer->expects($this->atLeastOnce())
            ->method('has')
            ->willReturnMap($hasMap);

        $mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('config'))
            ->willReturn($expected);

        $factory = new CacheFactory($serviceKey);
        $factory->getConfig($mockContainer);
    }

    public function testGetMapper()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $factory = new CacheFactory();
        $result = $factory->getMapper($mockContainer);
        $this->assertInstanceOf(AdapterMapper::class, $result);
    }

    public function testInvoke()
    {
        $type = 'array';
        $options = ['some-key' => 'some-value'];

        $factory = $this->getMockBuilder(CacheFactory::class)
            ->onlyMethods(['getConfig', 'getMapper'])
            ->getMock();

        $mockContainer = $this->createMock(ContainerInterface::class);

        $mockConfig = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockMapper = $this->getMockBuilder(AdapterMapper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockAdapter = $this->getMockBuilder(AdapterInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory->expects($this->once())
            ->method('getConfig')
            ->willReturn($mockConfig);

        $factory->expects($this->once())
            ->method('getMapper')
            ->willReturn($mockMapper);

        $mockConfig->expects($this->once())
            ->method('getType')
            ->willReturn($type);

        $mockConfig->expects($this->once())
            ->method('getOptions')
            ->willReturn($options);

        $mockMapper->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo($type),
                $this->equalTo($options)
            )->willReturn($mockAdapter);

        $result = $factory($mockContainer);

        $this->assertInstanceOf(AdapterInterface::class, $result);
    }

    public function testCallStatic()
    {
        $service = 'someCache';
        $type = 'some-service';

        $config = [
            'cache' => [
                $service => [
                    'type' => $type,
                    'options' => []
                ],
            ],
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);

        $mockAdapter = $this->getMockBuilder(AdapterInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $hasMap = [
            ['config', true],
            [$type, true]
        ];

        $mockContainer->expects($this->any())
            ->method('has')
            ->willReturnMap($hasMap);

        $getMap = [
            ['config', $config],
            [$type, $mockAdapter]
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($getMap);

        $result = CacheFactory::someCache($mockContainer);
        $this->assertInstanceOf(AdapterInterface::class, $result);
    }

    public function testCallStaticMissingContainer()
    {
        $this->expectException(InvalidContainerException::class);
        CacheFactory::someCache();
    }
}
