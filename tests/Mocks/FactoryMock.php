<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Test\Mocks;

use Blazon\PSR11SymfonyCache\Adapter\ContainerAwareInterface;
use Blazon\PSR11SymfonyCache\Adapter\FactoryInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

class FactoryMock implements FactoryInterface, ContainerAwareInterface
{
    /** @var ContainerInterface|null */
    public static $container = null;

    public function __invoke(array $options): AdapterInterface
    {
        return new ArrayAdapter();
    }

    public function getContainer(): ContainerInterface
    {
        return self::$container;
    }

    public function setContainer(ContainerInterface $container): void
    {
        self::$container = $container;
    }
}
