<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Test\Adaptor;

use Blazon\PSR11SymfonyCache\Adapter\APCuAdapterFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ApcuAdapter;

/**
 * @covers \Blazon\PSR11SymfonyCache\Adapter\APCuAdapterFactory
 */
class APCuAdapterFactoryTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory = new APCuAdapterFactory();
        $result = $factory([]);
        $this->assertInstanceOf(ApcuAdapter::class, $result);
    }

    public function testInvokeWithVersion(): void
    {
        $factory = new APCuAdapterFactory();
        $result = $factory(['version' => 'testing']);
        $this->assertInstanceOf(ApcuAdapter::class, $result);
    }

    public function testInvokeWithOptions(): void
    {
        $namespace = 'some-namespace';
        $lifetime = 4300;
        $version = 3;

        $options = [
            'namespace' => $namespace,
            'defaultLifetime' => $lifetime,
            'version' => $version
        ];

        $mockAdapter = $this->getMockBuilder(ApcuAdapter::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|APCuAdapterFactory $factory */
        $factory = $this->getMockBuilder(APCuAdapterFactory::class)
            ->onlyMethods(['getAdapter'])
            ->getMock();

        $factory->expects($this->once())
            ->method('getAdapter')
            ->with(
                $this->equalTo($namespace),
                $this->equalTo($lifetime),
                $this->equalTo($version)
            )->willReturn($mockAdapter);

        $result = $factory->__invoke($options);

        $this->assertEquals($mockAdapter, $result);
    }
}
