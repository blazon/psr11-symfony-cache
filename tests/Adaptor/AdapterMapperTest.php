<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Test\Adaptor;

use Blazon\PSR11SymfonyCache\Adapter\AdapterMapper;
use Blazon\PSR11SymfonyCache\Adapter\APCuAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\ArrayAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\ChainAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\CouchbaseAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\DoctrineAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\FilesystemAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\MemcachedAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\PdoAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\PhpArrayAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\PhpFilesAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\ProxyAdapterFactory;
use Blazon\PSR11SymfonyCache\Adapter\RedisAdapterFactory;
use Blazon\PSR11SymfonyCache\Exception\InvalidConfigException;
use Blazon\PSR11SymfonyCache\Test\Mocks\FactoryMock;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

/**
 * @covers \Blazon\PSR11SymfonyCache\Adapter\AdapterMapper
 */
class AdapterMapperTest extends TestCase
{
    /** @var AdapterMapper */
    protected $mapper = null;

    /** @var MockObject|ContainerInterface|null */
    protected $mockContainer = null;

    protected function setUp(): void
    {
        $this->mockContainer = $this->createMock(ContainerInterface::class);
        $this->mapper = new AdapterMapper($this->mockContainer);

        // Reset mock factory
        FactoryMock::$container = null;

        $this->assertInstanceOf(AdapterMapper::class, $this->mapper);
    }

    public function testConstructor(): void
    {
    }

    public function testGetFactoryClassNameNoClassExists(): void
    {
        $result = $this->mapper->getFactoryClassName('DoesNotExist');
        $this->assertNull($result);
    }

    public function testGetFactoryClassNameAPCu(): void
    {
        $result = $this->mapper->getFactoryClassName('apcu');
        $this->assertEquals(APCuAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('APCu');
        $this->assertEquals(APCuAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameArray(): void
    {
        $result = $this->mapper->getFactoryClassName('array');
        $this->assertEquals(ArrayAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('ARRAY');
        $this->assertEquals(ArrayAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameChain(): void
    {
        $result = $this->mapper->getFactoryClassName('chain');
        $this->assertEquals(ChainAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('CHAIN');
        $this->assertEquals(ChainAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameCouchBase(): void
    {
        $result = $this->mapper->getFactoryClassName('couchbase');
        $this->assertEquals(CouchbaseAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('COUCHBASE');
        $this->assertEquals(CouchbaseAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameDoctrine(): void
    {
        $result = $this->mapper->getFactoryClassName('doctrine');
        $this->assertEquals(DoctrineAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('DOCTRINE');
        $this->assertEquals(DoctrineAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameFilesystem(): void
    {
        $result = $this->mapper->getFactoryClassName('filesystem');
        $this->assertEquals(FilesystemAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('FILESYSTEM');
        $this->assertEquals(FilesystemAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameMemcached(): void
    {
        $result = $this->mapper->getFactoryClassName('memcached');
        $this->assertEquals(MemcachedAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('MEMCACHED');
        $this->assertEquals(MemcachedAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNamePDO(): void
    {
        $result = $this->mapper->getFactoryClassName('pdo');
        $this->assertEquals(PdoAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('PDO');
        $this->assertEquals(PdoAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNamePhpArray(): void
    {
        $result = $this->mapper->getFactoryClassName('phparray');
        $this->assertEquals(PhpArrayAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('PHPARRAY');
        $this->assertEquals(PhpArrayAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNamePhpFiles(): void
    {
        $result = $this->mapper->getFactoryClassName('phpfiles');
        $this->assertEquals(PhpFilesAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('PHPFILES');
        $this->assertEquals(PhpFilesAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameProxy(): void
    {
        $result = $this->mapper->getFactoryClassName('proxy');
        $this->assertEquals(ProxyAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('PROXY');
        $this->assertEquals(ProxyAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameRedis(): void
    {
        $result = $this->mapper->getFactoryClassName('redis');
        $this->assertEquals(RedisAdapterFactory::class, $result);

        $result = $this->mapper->getFactoryClassName('REDIS');
        $this->assertEquals(RedisAdapterFactory::class, $result);
    }

    public function testGetFactoryClassNameFactoryMock(): void
    {
        $result = $this->mapper->getFactoryClassName(FactoryMock::class);
        $this->assertEquals(FactoryMock::class, $result);
    }

    public function testGet(): void
    {
        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(null);

        $this->mockContainer->expects($this->never())
            ->method('get');

        $result = $this->mapper->get(FactoryMock::class, []);

        $this->assertInstanceOf(ArrayAdapter::class, $result);
        $this->assertEquals($this->mockContainer, FactoryMock::$container);
    }

    public function testGetInvalidClass(): void
    {
        $this->expectException(InvalidConfigException::class);

        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(null);

        $this->mockContainer->expects($this->never())
            ->method('get');

        $this->mapper->get(TestCase::class, []);
    }

    public function testGetWithUnknownClass(): void
    {
        $this->expectException(InvalidConfigException::class);

        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(null);

        $this->mockContainer->expects($this->never())
            ->method('get');

        $this->mapper->get('DoesNotExist', []);
    }

    public function testGetWithContainerService(): void
    {
        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(true);

        $mockAdaptor = $this->createMock(ArrayAdapter::class);

        $this->mockContainer->expects($this->once())
            ->method('get')
            ->willReturn($mockAdaptor);

        $result = $this->mapper->get('MyService', []);

        $this->assertEquals($mockAdaptor, $result);
    }

    public function testHas(): void
    {
        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(false);

        $result = $this->mapper->has(TestCase::class);

        $this->assertTrue($result);
    }

    public function testHasClassNotFound(): void
    {
        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(false);

        $result = $this->mapper->has('DoesNotExist');

        $this->assertFalse($result);
    }

    public function testHasService(): void
    {
        $this->mockContainer->expects($this->once())
            ->method('has')
            ->willReturn(true);

        $result = $this->mapper->has('MyService');

        $this->assertTrue($result);
    }
}
