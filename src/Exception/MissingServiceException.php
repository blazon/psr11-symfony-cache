<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Exception;

use OutOfBoundsException;

class MissingServiceException extends OutOfBoundsException
{
}
