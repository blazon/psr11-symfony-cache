<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache;

use Blazon\PSR11SymfonyCache\Adapter\AdapterMapper;
use Blazon\PSR11SymfonyCache\Adapter\MapperInterface;
use Blazon\PSR11SymfonyCache\Config\Config;
use Blazon\PSR11SymfonyCache\Exception\InvalidConfigException;
use Blazon\PSR11SymfonyCache\Exception\InvalidContainerException;
use Blazon\PSR11SymfonyCache\Exception\MissingConfigException;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class CacheFactory
{
    protected $configKey = 'default';

    /** @psalm-suppress MissingParamType */
    public static function __callStatic($name, $arguments): AdapterInterface
    {
        if (
            empty($arguments[0])
            || !$arguments[0] instanceof ContainerInterface
        ) {
            throw new InvalidContainerException(
                'Argument 0 must be an instance of a PSR-11 container'
            );
        }

        $factory = new self($name);
        return $factory($arguments[0]);
    }

    public function __construct(string $configKey = 'default')
    {
        $this->configKey = $configKey;
    }

    public function __invoke(ContainerInterface $container): AdapterInterface
    {
        $config = $this->getConfig($container);
        $mapper = $this->getMapper($container);
        return $mapper->get($config->getType(), $config->getOptions());
    }

    public function getMapper(ContainerInterface $container): MapperInterface
    {
        return new AdapterMapper($container);
    }

    public function getConfig(ContainerInterface $container): Config
    {
        $config = $this->getConfigArray($container);

        if (empty($config['cache'][$this->configKey])) {
            throw new InvalidConfigException(
                "No config found for adapter: " . $this->configKey
            );
        }

        return new Config($config['cache'][$this->configKey]);
    }

    public function getConfigArray(ContainerInterface $container): array
    {
        // Symfony config is parameters. //
        if (
            method_exists($container, 'getParameter')
            && method_exists($container, 'hasParameter')
            && $container->hasParameter('cache')
        ) {
            return ['cache' => $container->getParameter('cache')];
        }

        // Zend uses config key
        if ($container->has('config')) {
            return $container->get('config');
        }

        // Slim Config comes from "settings"
        if ($container->has('settings')) {
            return ['cache' => $container->get('settings')['cache']];
        }

        throw new MissingConfigException("Unable to locate Cache configuration");
    }
}
