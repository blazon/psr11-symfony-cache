<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Adapter;

use Symfony\Component\Cache\Adapter\AdapterInterface;

interface FactoryInterface
{
    public function __invoke(array $options): AdapterInterface;
}
