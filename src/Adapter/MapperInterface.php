<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Adapter;

use Symfony\Component\Cache\Adapter\AdapterInterface;

interface MapperInterface
{
    public function get(string $type, array $options): AdapterInterface;
    public function has(string $type): bool;
    public function getFactoryClassName(string $type): ?string;
}
