<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Adapter;

use Psr\Container\ContainerInterface;
use Blazon\PSR11SymfonyCache\Exception\InvalidConfigException;
use Symfony\Component\Cache\Adapter\AdapterInterface;

/** @SuppressWarnings(PHPMD.CouplingBetweenObjects) */
class AdapterMapper implements MapperInterface
{
    /** @var ContainerInterface */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /** @SuppressWarnings(PHPMD.CyclomaticComplexity) */
    public function getFactoryClassName(string $type): ?string
    {
        if (
            strtolower($type) !== 'pdo'
            && strtolower($type) !== 'memcached'
            && strtolower($type) !== 'redis'
            && class_exists($type)
        ) {
            return $type;
        }

        switch (strtolower($type)) {
            case 'apcu':
                return APCuAdapterFactory::class;
            case 'array':
                return ArrayAdapterFactory::class;
            case 'chain':
                return ChainAdapterFactory::class;
            case 'couchbase':
                return CouchbaseAdapterFactory::class;
            case 'doctrine':
                return DoctrineAdapterFactory::class;
            case 'filesystem':
                return FilesystemAdapterFactory::class;
            case 'memcached':
                return MemcachedAdapterFactory::class;
            case 'pdo':
                return PdoAdapterFactory::class;
            case 'phparray':
                return PhpArrayAdapterFactory::class;
            case 'phpfiles':
                return PhpFilesAdapterFactory::class;
            case 'proxy':
                return ProxyAdapterFactory::class;
            case 'redis':
                return RedisAdapterFactory::class;
        }

        return null;
    }

    public function get(string $type, array $options): AdapterInterface
    {
        if ($this->container->has($type)) {
            return $this->container->get($type);
        }

        $className = $this->getFactoryClassName($type);

        if (!$className) {
            throw new InvalidConfigException(
                'Unable to locate a factory by the name of: ' . $type
            );
        }

        if (!in_array(FactoryInterface::class, class_implements($className))) {
            throw new InvalidConfigException(
                'Class ' . $className . ' must be an instance of ' . FactoryInterface::class
            );
        }

        /** @var FactoryInterface $factory */
        $factory = new $className();

        if ($factory instanceof ContainerAwareInterface) {
            $factory->setContainer($this->container);
        }

        // @codeCoverageIgnoreStart
        // Unreachable code in tests
        if (!is_callable($factory)) {
            throw new InvalidConfigException(
                'Class ' . $className . ' must be callable.'
            );
        }
        // @codeCoverageIgnoreEnd

        return $factory($options);
    }

    public function has(string $type): bool
    {
        if ($this->container->has($type)) {
            return true;
        }

        $className = $this->getFactoryClassName($type);

        if (!$className) {
            return false;
        }

        return true;
    }
}
