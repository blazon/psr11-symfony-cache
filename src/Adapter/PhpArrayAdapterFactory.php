<?php

declare(strict_types=1);

namespace Blazon\PSR11SymfonyCache\Adapter;

use Blazon\PSR11SymfonyCache\Exception\InvalidConfigException;
use Blazon\PSR11SymfonyCache\Exception\MissingConfigException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\PhpArrayAdapter;

class PhpArrayAdapterFactory implements FactoryInterface, ContainerAwareInterface
{
    use ContainerTrait;

    public function __invoke(array $options): AdapterInterface
    {
        $backupCache = (string) ($options['backupCache'] ?? null);
        $filePath = (string) ($options['filePath'] ?? '');

        if (empty($backupCache)) {
            throw new MissingConfigException(
                'A backup cache service is required for the php array adapter'
            );
        }

        if (empty($filePath)) {
            throw new MissingConfigException(
                'A file path is required for the php array adapter'
            );
        }

        if (!$this->getContainer()->has($backupCache)) {
            throw new InvalidConfigException(
                'No service found by the name: ' . $backupCache
            );
        }

        /** @var AdapterInterface $cacheService */
        $cacheService = $this->getContainer()->get($backupCache);

        return new PhpArrayAdapter($filePath, $cacheService);
    }
}
